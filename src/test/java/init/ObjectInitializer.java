package init;

import org.openqa.selenium.WebDriver;

import pages.LoginPage;
import pages.RoomPage;

public class ObjectInitializer {
	public LoginPage lPage;
	public RoomPage rPage;

	public ObjectInitializer(WebDriver driver) {
		lPage = new LoginPage(driver);
		rPage = new RoomPage(driver);
	}
}
