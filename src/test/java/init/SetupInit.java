package init;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;

public class SetupInit {

	protected enum Condition {
		isDisplayed, isClickable, isPresent, isNotVisible
	}

	// private Object DOWNLOADS_FOLDER;
	public WebDriver driver;
	protected String testURL = "https://automationintesting.online/#/admin";
	private int MAX_WAIT_TIME_IN_SEC = 30;
	public static final String TEST_DATA_FILE = "./data-provider/createRoom.xls";
	public static String browser;
	public RequestSpecification request;

	@BeforeSuite
	public void setUp() {
		if (System.getProperty("module").equalsIgnoreCase("web")) {
			driver = initWebDriver();
			driver.get(testURL);
		} else if (System.getProperty("module").equalsIgnoreCase("api")) {
			RestAssured.reset();
			request = RestAssured.given();
		}
	}

	public WebDriver getDriver() {
		return this.driver;
	}

	public RequestSpecification getRequestSpecification() {
		return request;
	}

	@AfterSuite
	public void closeBrowser() {
		if (System.getProperty("module").equalsIgnoreCase("web"))
			driver.quit();
	}

	private WebDriver initWebDriver() {
		if (browser == null)
			browser = System.getProperty("browser");
		switch (browser.toLowerCase()) {
		case "chrome":
			System.out.println("Launching google chrome with existing profile..");
			WebDriverManager.chromedriver().setup();
			ChromeOptions chromeOption = setChromeOptions();
			return new ChromeDriver(chromeOption);
		case "edge":
			System.out.println("Launching edge with existing profile..");
			WebDriverManager.edgedriver().setup();
			return new EdgeDriver();
		case "firefox":
			System.out.println("Launching firefox with existing profile..");
			WebDriverManager.firefoxdriver().setup();
			return new FirefoxDriver();
		default:
			System.out.println("Launching google chrome with existing profile..");
			WebDriverManager.chromedriver().setup();
			ChromeOptions option = setChromeOptions();
			return new ChromeDriver(option);
		}
	}

	private ChromeOptions setChromeOptions() {
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default_content_settings.popups", Integer.valueOf(0));
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		options.addArguments("--disable-notifications");
		options.addArguments("--disable-extenstions");
		options.addArguments(new String[] { "disable-infobars" });
		options.setExperimentalOption("prefs", chromePrefs);
		DesiredCapabilities capabilities = setChromeCapabilities();
		options.merge(capabilities);
		return options;
	}

	private DesiredCapabilities setChromeCapabilities() {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("acceptSslCerts", true);
		return capabilities;
	}

	public void clickOnElement(By locator, int... timeOrAssert) {
		WebElement element = null;
		Map<WebElement, String> elementState = new HashMap<>();
		elementState = waitForElementState(locator, Condition.isDisplayed, getTimeOut(timeOrAssert));
		for (Map.Entry<WebElement, String> entry : elementState.entrySet()) {
			element = entry.getKey();
		}
		try {
			if (element == null)
				throw new Exception();
			else
				element.click();
		} catch (Exception e) {
//			e.printStackTrace();
			exceptionOnFailure(false, e.toString(), timeOrAssert);
		}
	}

	public void exceptionOnFailure(boolean success, String message, int[] assertion) {
		if (!success) {
			if (assertionResult(assertion)) {
				try {
					assertStatus(success);
				} catch (Exception e) {
					RuntimeException ex = new RuntimeException(message + " : " + e.getMessage());
//					System.out.println("Exception Logging For: " + message);
					ex.setStackTrace(e.getStackTrace());
					throw ex;
				}
			}
		}
	}

	public boolean assertionResult(int[] j) {
		if (j != null) {
			if (j.length > 0) {
				if (j[0] != 0) {
					return false;
				} else {
					return true;
				}
			}
		}
		return false;
	}

	public void assertStatus(boolean success) throws Exception {
		if (!success)
			throw new Exception("");
	}

	private Map<WebElement, String> waitForElementState(By locator, Condition condition, int time) {
		WebElement element;
		Map<WebElement, String> map = new HashMap<>();
		element = getElement(condition, locator, time);
		String message = "";
		if (element == null) {
			try {
				throw new Exception();
			} catch (Exception e) {
				message = "State = " + condition.toString() + " failed: ";
			}
		} else {
			message = "State = " + condition.toString() + " Passed: ";
		}
		map.put(element, message);
		return map;

	}

	private WebElement getElement(Condition condition, By by, int time) {
		WebElement element = null;
		@SuppressWarnings("deprecation")
		WebDriverWait wait = new WebDriverWait(driver, time);
		try {
			switch (condition) {
			case isClickable:
				element = (WebElement) wait.until(ExpectedConditions.visibilityOfElementLocated(by));
				if (element == null) {
					return element;
				} else if (element.getAttribute("clickable") == null) {
					return element;
				} else if (element.getAttribute("clickable") != null) {
					element = (WebElement) wait.until(ExpectedConditions.elementToBeClickable(by));
					return element;
				}
				break;
			case isDisplayed:
				element = (WebElement) wait.until(ExpectedConditions.visibilityOfElementLocated(by));
				break;
			case isPresent:
				element = (WebElement) wait.until(ExpectedConditions.presenceOfElementLocated(by));
				break;
			default:
				break;
			}
		} catch (Exception e) {
		}
		return element;
	}

	private int getTimeOut(int[] time) {
		int timeOut = MAX_WAIT_TIME_IN_SEC;
		if (time.length != 0)
			if (time[0] > 0)
				timeOut = time[0];
		return timeOut;
	}

	public Instant getCurrentTime() {
		return Instant.now();
	}

	protected boolean isVisibleInViewport(WebElement element) {
		return ((Boolean) ((JavascriptExecutor) ((RemoteWebElement) element).getWrappedDriver()).executeScript(
				"var elem = arguments[0],                   box = elem.getBoundingClientRect(),      cx = box.left + box.width / 2,           cy = box.top + box.height / 2,           e = document.elementFromPoint(cx, cy); for (; e; e = e.parentElement) {           if (e === elem)                            return true;                         }                                        return false;                            ",
				new Object[] { element })).booleanValue();
	}

	public boolean isLoderDisplayed(By locator) {
		boolean state = false;
		try {
			state = driver.findElement(locator).isDisplayed();
		} catch (Exception e) {
			state = false;
		}
		return state;
	}

	public void sendKeysWithClear(By locator, String data, int... timeOrAssert) {
		WebElement element = null;
		Map<WebElement, String> elementState = new HashMap<>();
		elementState = waitForElementState(locator, Condition.isDisplayed, getTimeOut(timeOrAssert));
		for (Map.Entry<WebElement, String> entry : elementState.entrySet()) {
			element = entry.getKey();
		}
		try {
			if (element == null)
				throw new Exception();
			else {
				element.clear();
				element.sendKeys(data);
			}
		} catch (Exception e) {
			exceptionOnFailure(false, e.toString(), timeOrAssert);
		}
	}

	public boolean isElementDisplay(By locator, int... timeOrAssert) {
		WebElement element = null;
		boolean status = false;
		Map<WebElement, String> elementState = new HashMap<>();
		elementState = waitForElementState(locator, Condition.isDisplayed, getTimeOut(timeOrAssert));
		for (Map.Entry<WebElement, String> entry : elementState.entrySet()) {
			element = entry.getKey();
		}
		try {
			if (element == null)
				throw new Exception();
			else {
				status = true;
			}
		} catch (Exception e) {
			status = false;
			exceptionOnFailure(false, e.toString(), timeOrAssert);
		}
		return status;
	}

	public void selectFromDropdown(By locator, String data, int... timeOrAssert) {
		WebElement element = null;
		Map<WebElement, String> elementState = new HashMap<>();
		elementState = waitForElementState(locator, Condition.isDisplayed, getTimeOut(timeOrAssert));
		for (Map.Entry<WebElement, String> entry : elementState.entrySet()) {
			element = entry.getKey();
		}
		try {
			if (element == null)
				throw new Exception();
			else {
				Select select = new Select(element);
				select.selectByVisibleText(data);
			}
		} catch (Exception e) {
			exceptionOnFailure(false, e.toString(), timeOrAssert);
		}
	}

	public void sendKeys(By locator, String data, int... timeOrAssert) {
		WebElement element = null;
		Map<WebElement, String> elementState = new HashMap<>();
		elementState = waitForElementState(locator, Condition.isDisplayed, getTimeOut(timeOrAssert));
		for (Map.Entry<WebElement, String> entry : elementState.entrySet()) {
			element = entry.getKey();
		}
		try {
			if (element == null)
				throw new Exception();
			else {
				element.clear();
				element.sendKeys(data);
			}
		} catch (Exception e) {
			exceptionOnFailure(false, e.toString(), timeOrAssert);
		}
	}

	public void sendPresentKeys(By locator, String data, int... timeOrAssert) {
		WebElement element = null;
		Map<WebElement, String> elementState = new HashMap<>();
		elementState = waitForElementState(locator, Condition.isPresent, getTimeOut(timeOrAssert));
		for (Map.Entry<WebElement, String> entry : elementState.entrySet()) {
			element = entry.getKey();
		}
		try {
			if (element == null)
				throw new Exception();
			else
				element.sendKeys(data);
		} catch (Exception e) {
			exceptionOnFailure(false, e.toString(), timeOrAssert);
		}
	}

	public boolean verifyVisible(By locator, int... timeOrAssert) {
		WebElement element = findVisibleElement(locator, timeOrAssert);
		return element != null ? element.isDisplayed() : false;
	}

	public WebElement findVisibleElement(By locator, int... timeOrAssert) {
		WebElement element = null;
		Map<WebElement, String> elementState = new HashMap<>();
		elementState = waitForElementState(locator, Condition.isDisplayed, getTimeOut(timeOrAssert));
		for (Map.Entry<WebElement, String> entry : elementState.entrySet()) {
			element = entry.getKey();
		}
		try {
			if (element == null)
				throw new Exception();
		} catch (Exception e) {
			exceptionOnFailure(false, e.toString(), timeOrAssert);
		}
		return element;
	}

	public void log(String message) {
		commonWait();
		Reporter.log(message + "<br/>");
	}

	public void commonWait() {
		try {
			Thread.sleep(400);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void commonWait(int sec) {
		try {
			Thread.sleep(1000 * sec);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public String getElementText(By locator, int... timeOrAssert) {
		WebElement element = null;
		Map<WebElement, String> elementState = new HashMap<>();
		elementState = waitForElementState(locator, Condition.isDisplayed, getTimeOut(timeOrAssert));
		for (Map.Entry<WebElement, String> entry : elementState.entrySet()) {
			element = entry.getKey();
		}
		try {
			if (element == null)
				throw new Exception();
		} catch (Exception e) {
			exceptionOnFailure(false, e.toString(), timeOrAssert);
		}
		return element.getText();
	}

	public void nevigateBack() {
		commonWait(5);
		driver.navigate().back();
	}

}
