package testcase.web;

import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import init.ObjectInitializer;
import init.SetupInit;
import init.TestDataImport;

public class Rooms extends SetupInit {
	ObjectInitializer oi;

	@BeforeClass
	public void beforeSetup() {
		oi = new ObjectInitializer(getDriver());
		oi.lPage.login("admin", "password", 0);
	}

	@Test(dataProvider = "createRoom", dataProviderClass = TestDataImport.class, description = "Id: Create Room")
	public void createRoom(Map<Object, Object> map) {
		oi.rPage.createRoom(map, 0);
		Assert.assertTrue(oi.rPage.verifyCreatedRoom(map, 5));
	}
}
