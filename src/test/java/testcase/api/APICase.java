package testcase.api;

import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import init.SetupInit;
import io.restassured.response.Response;

public class APICase extends SetupInit {
	public static String URL = "https://jsonplaceholder.typicode.com/todos/";

	@Test
	public void apiCase() {
		Response response = getRequestSpecification().when().log().all().get(URL);
		response.then().log().all().statusCode(200);
		List<Map<String, Object>> responseList = response.path("");
		for (Map<String, Object> map : responseList) {
			if (map.get("id").toString().equalsIgnoreCase("2")) {
				Assert.assertFalse(Boolean.parseBoolean(map.get("completed").toString()));
				break;
			}
		}
	}
}
