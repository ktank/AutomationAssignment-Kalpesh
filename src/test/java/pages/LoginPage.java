package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import init.SetupInit;

public class LoginPage extends SetupInit {
	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}

	By txtUsername = By.id("username");

	By txtPassword = By.id("password");

	By btnLogin = By.id("doLogin");

	private void enterUsername(String username, int... time) {
		sendKeysWithClear(txtUsername, username, time);
	}

	private void enterPassword(String password, int[] time) {
		sendKeysWithClear(txtPassword, password, time);
	}

	private void clickOnLogin(int[] time) {
		clickOnElement(btnLogin, time);
	}

	public void login(String username, String password, int... time) {
		enterUsername(username, time);
		enterPassword(password, time);
		clickOnLogin(time);
	}
}
