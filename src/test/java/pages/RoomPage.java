package pages;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import init.SetupInit;

public class RoomPage extends SetupInit {
	public RoomPage(WebDriver driver) {
		this.driver = driver;
	}

	By txtRoomName = By.id("roomName");

	By drpType = By.id("type");

	By drpAccessible = By.id("accessible");

	By txtRoomPrice = By.id("roomPrice");

	By btnCreate = By.id("createRoom");

	String checkBox = "//input[@value='%s']";

	String roomName = "(//*[@id='roomName%s'])[last()]";

	String roomType = "(//*[@id='type%s'])[last()]";

	String accessible = "(//*[@id='accessible%s'])[last()]";

	String roomPrice = "(//*[@id='roomPrice%s'])[last()]";

	private void enterRoomName(String roomName, int... time) {
		sendKeysWithClear(txtRoomName, roomName, time);
	}

	private void selectRoomType(String roomType, int... time) {
		selectFromDropdown(drpType, roomType, time);
	}

	private void selectAccessible(String accessible, int... time) {
		selectFromDropdown(drpAccessible, accessible, time);
	}

	private void enterRoomPrice(String roomPrice, int... time) {
		sendKeysWithClear(txtRoomPrice, roomPrice, time);
	}

	private void selectRoomDetails(String[] roomDetails, int... time) {
		for (int i = 0; i < roomDetails.length; i++) {
			clickOnElement(By.xpath(String.format(checkBox, roomDetails[i])), time);
		}
	}

	private void clickOnCreateRoom(int... time) {
		clickOnElement(btnCreate, time);
	}

	public void createRoom(Map<Object, Object> map, int... time) {
		enterRoomName(map.get("roomname").toString(), time);
		selectRoomType(map.get("roomtype").toString(), time);
		selectAccessible(map.get("accessible").toString(), time);
		enterRoomPrice(map.get("price").toString(), time);
		selectRoomDetails(map.get("roomdetails").toString().split(","), time);
		clickOnCreateRoom(time);
	}

	public boolean verifyCreatedRoom(Map<Object, Object> map, int... time) {
		boolean status = true;
		if (!isElementDisplay(By.xpath(String.format(roomName, map.get("roomname").toString())), time))
			status = false;
		if (!isElementDisplay(By.xpath(String.format(roomType, map.get("roomtype").toString())), time))
			status = false;
		if (!isElementDisplay(By.xpath(String.format(accessible, map.get("accessible").toString())), time))
			status = false;
		if (!isElementDisplay(By.xpath(String.format(roomPrice, map.get("price").toString())), time))
			status = false;
		return status;
	}
}
